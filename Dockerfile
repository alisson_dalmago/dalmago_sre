FROM nginx:1.14.2-alpine

COPY public_html /public_html
COPY conf.d /etc/nginx/conf.d/
COPY dhparam /etc/nginx/dhparam
COPY certbot/conf/ /etc/nginx/ssl/

RUN chmod 755 public_html/cv.pdf
CMD ["nginx", "-g", "daemon off;"]
